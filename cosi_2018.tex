\documentclass[letterpaper,10pt]{article}
\usepackage{osameet2}

%% standard packages and arguments should be modified as needed
\usepackage{amsmath,amssymb}

\usepackage[pdftex,colorlinks=true,bookmarks=false,citecolor=blue,urlcolor=blue]{hyperref} %pdflatex
%\usepackage[dvips,colorlinks=true,bookmarks=false,citecolor=blue,urlcolor=blue]{hyperref} %latex w/dvips

% custom defined symbols
\newcommand{\mb}{\mathbf}

% custom packages used
\usepackage{mathrsfs,cleveref}

%\usepackage{authblk}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,xspace}

\begin{document}

\title{Sampling and processing for multiple scattering in inline compressive holography}

\author{Waleed Tahir$^{1}$, Ulugbek~S.~Kamilov$^{2}$, Lei Tian$^{1}$}
\address{$^1$ Department of Electrical and Computer Engineering, Boston University\\
	$^2$ Department of Computer Science and Engineering, Washington University in St. Louis}

\email{waleedt@bu.edu, kamilov@wustl.edu, leitian@bu.edu }

\begin{abstract}
Inline holography is approached from a computational perspective by incorporating a nonlinear forward model based on the iterative Born approximation (IBA). Sampling and its effects on multiple scattering computations are discussed. 
\end{abstract}

\ocis{(290.4210) Multiple scattering, (110.1758) Computational imaging, (090.0090) Holography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%
%%%% Brief introduction
%
Accounting for multiple scattering in holography is a challenging task. Most of the works in this domain~\cite{Brady2009b} account for single scattering only, thus formulating a linear relationship between a sparse 3D object and its measured 2D hologram. Although this might be an accurate assumption when the object is weakly scattering and sparse, it has been shown that as the permittivity contrast and/or density of the object increases, the effect of multiple scattering cannot be neglected to obtain accurate reconstructions~\cite{Wang1989,Kamilov.etal2016a}. In a previous work~\cite{Tahir2017}, we formulated inverse multiple scattering for inline holography based on the iterative Born approximation (IBA). In this work, we highlight some of the considerations that have to be made with regards to the sampling of the 3D field within the object in order to effectively account for multiple scattering in large scale problems.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Theoretical Background}
%
%%%% Theoretical basics
%
The intensity captured with an inline hologram of a 3D object can be written as $I(\vec{x}) = |u_{in}(\vec{x},z=0)+E(\vec{x})|^2$, where $\vec{x}$ represents transverse spatial coordinates, $z$ the axial depth, $u_{in}$ the incident field, and $E$ the scattered field on the plane of measurement $(z=0)$. Assume illumination from a known plane wave source with $u_{in}(\vec{x},0)=A$. If $f(\vec{x},z)$ represents the object scattering density with support $\Omega$, and $h(\vec{x},z)$ the out-going Green's function, and $u_K(\vec{x},z)$ the $K^{th}$ order multiply-scattered field within the object, then the scattered field at the image plane can be approximated by the integral
$E(\vec{x})=\iint_{\Omega} f(\vec{x}',z') u_K(\vec{x}',z') h(\vec{x}-\vec{x}',0-z') \,d\vec{x}'\,dz'$, 
where $u_K$ is given under IBA as 
$u_K(\vec{x},z)=u_{in}(\vec{x},z)+\iint_{\Omega} f(\vec{x}',z') \mu_K(\vec{x}',z') h(\vec{x}-\vec{x}',z-z') \,d\vec{x}'\,dz'$. 
These two equations can be discretized to give us the forward model for our system as
%
%%%% Forward model equations
%
\begin{align}
\mb{E} &= H(\mb{u}_K \odot \mb{f})\label{e1}, \\
\mb{u}_k &= \mb{u}_{in} + G(\mb{u}_{k-1} \odot \mb{f}).\label{e2}
\end{align}
%
%%%% Explanation of notation
%
Here $\mb{f}$ and $\mb{u}_k$ are vectors with dimensions $(N_x \times N_y \times N_z) \times 1$ and $\mb{E}$ with dimension $(N_x \times N_y) \times 1$, where $N_x$, $N_y$, and $N_z$ are the number of pixels in the $x$, $y$, and $z$ coordinates respectively. $\odot$ is element-wise multiplication. $H$ and $G$ are propagation operators. $H = K^H Q_0 B$, where K is the 2D DFT matrix of size $(N_x \times N_Y) \times (N_x \times N_Y)$, $K^H$ its Hermitian, $B = \mathrm{bldiag}(K,..,K)$ is a block-diagonal matrix, and $Q_0 = [L_{0-1} L_{0-2} ... L_{0-N_z}]$. $L_{z-z'}$ is the discretized depth dependent transfer function representing propagation between two slices from $z'$ to $z$. $G = B^H R B$, where $R = [Q_1, Q_2, ..., Q_{N_z}]$. $L_{z-z'}$ is given by
$L_{z-z'} = \dot{\iota}\pi \exp\{\dot{\iota}2\pi\sqrt{1/\lambda^2 - {\Delta_k}^2(p^2+q^2)}|(z-z')\Delta_z|\} \big/ (\lambda^2 \sqrt{1/\lambda^2 - {\Delta_k}^2(p^2+q^2)})$, 
where $\Delta_k$ and $\Delta_z$ are the discretization step-sizes in lateral frequency and along the optical axis respectively. $p$ and $q$ are the discrete variables for lateral frequencies.  The inverse scattering can then be formulated as an optimization problem.  Conveniently, the gradient computation is a $K^{th}$-order recursion, as discussed in~\cite{Tahir2017,Kamilov.etal2016a}.

%
%%%% Intuitive interpretation of H & G
%
It can be observed that $H$ compresses the volumetric information from $\Omega$ to a 2D image; $G$ carries 3D multiple scattering information by calculating higher-order propagation from $\Omega$ to within itself. If multiple scattering from eq.~\eqref{e2} is neglected, then eq.~\eqref{e1} reduces to the traditional holography formulation in where $\mb{u}_K$ is considered to be $\mb{u_{in}}$.  Incorporating multiple scattering is computationally demanding not only due to computation of higher order 3D fields, but it also places stringent requirements on the sampling of $\Omega$, which can be challenging when imaging at large scale where fine sampling at e.g. $\lambda/4$ is not computationally tractable. In such situations, the sampling requirements are also dictated by the computation method, i.e. whether the Green's function is sampled in the Fourier domain (spectrum method) or the spatial domain (direct method). A similar discussion can be found in~\cite{Mas1999}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and Discussion}
%
%%%% Discussion on aliasing wrt sampling and computation method
%
The effect of sampling size and computation method on the $H$ and $G$ operators are discussed in this section. To observe the effect of sampling size, we have two simulated experimental setups. In the first case, the lateral sampling size is $3.45\mu m$, which corresponds to the sensor size of the camera that we plan to use for experiment, while in the second case, the sampling size is $0.345\mu m$ which corresponds to an added 10$\times$ magnification. A theoretical analysis of the sampling on the lines of~\cite{Mas1999} has been performed but has not been included for brevity. However, it can be empirically observed from Fig.~\ref{fig1} that while the Green's function of the single scattering operator does not have any visibly significant aliasing artifacts for either setup or computation method, the multiple scattering operator is highly aliased in the first setup for both spectrum and direct methods, while for the second setup with finer sampling, the direct method approach is preferred. This also highlights some of the additional challenges that arise when having to account for multiple scattering. Further investigation is underway, and we expect to experimentally demonstrate the impact and considerations of incorporating multiple scattering in inline holography.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}[!ht]
	\centering
	\setlength{\belowcaptionskip}{-10pt}
	\includegraphics[width=0.8\linewidth]{fig_1}
	\caption{The numerically calculated Green's function of single (left) and multiple (right) scattering operators using different sampling rates and computation methods to demonstrate their empirical effects. The multiple scattering operator is more sensitive to these variables. This is due to more stringent sampling requirements that arise as a result of small propagation distance and high angle propagation within the sample volume.} 
	\label{fig1}
\end{figure*} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{References}
\bibliographystyle{osajnl}
\bibliography{Bib_v20} 


\end{document}
